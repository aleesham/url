package org.launchcode.training;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class UrlTest {
    Url urlLower, urlMixed, newUrl;

    @Before
    public void beforeAnything(){
        urlLower = new Url("https://launchcode.org/learn/longer-path/?user=1#fragment");
        urlMixed = new Url("fTP://launchCODe.org/learn?user=1&name=Aleesha");
    }

    //TODO: Test the constructor with lowercase string: use the getters to do this.
    @Test
    public void constructorProto(){
        assertEquals("https", urlLower.getProtocol());
    }

    @Test
    public void constructorDomain(){
        assertEquals("launchcode.org", urlLower.getDomain());
    }

    @Test
    public void constructorPort(){
        assertEquals("443", urlLower.getPort());
    }

    @Test
    public void constructorPath(){
        assertEquals("/learn/longer-path/", urlLower.getPath());
    }

    @Test
    public void constructorQuery(){
        assertEquals("user=1", urlLower.getQuery());
    }

    @Test
    public void constructorFragment(){
        assertEquals("fragment", urlLower.getFragment());
    }

    // Test toString
    @Test
    public void toStringTestWithoutPortQueryOrFragment(){
        newUrl = new Url("http://facebook.org/path");
        assertEquals("http://facebook.org/path", newUrl.toString());
    }

    @Test
    public void toStringTestWithPortQueryAndFragment(){
        newUrl = new Url("https://localhost:8888/learn?user=1#fragment");
        assertEquals("https://localhost:8888/learn?user=1#fragment", newUrl.toString());
    }

    //test helper function
    @Test
    public void usesDefaultPort(){
        assertTrue(urlLower.usesDefaultPort());
    }

    @Test
    public void usesCustomPort(){
        newUrl = new Url("https://localhost:8888/learn?user=1#fragment");
        assertFalse(newUrl.usesDefaultPort());
    }

    //test ports
    @Test
    public void defaultFtpPort(){
        assertEquals("21", urlMixed.getPort());
    }

    @Test
    public void defaultHttpPort(){
        newUrl = new Url("http://domain/path");
        assertEquals("80", newUrl.getPort());
    }

    @Test
    public void defaultHttpsPort(){
        assertEquals("443", urlLower.getPort());
    }

    @Test
    public void defaultFilePort(){
        newUrl = new Url("file://domain/path");
        assertEquals("21", newUrl.getPort());
    }

    @Test
    public void givenPort(){
        Url newUrl = new Url("https://localhost:8888/path");
        assertEquals("8888", newUrl.getPort());
    }



    //TODO: Throw an error if protocol is not ftp, http, https, or file
    @Test(expected = IllegalArgumentException.class)
    public void badProtocol(){
        Url newUrl = new Url("htp://launchcode.org/hi");
    }

    //TODO: Throw an error if protocol is empty
    @Test(expected = IllegalArgumentException.class)
    public void emptyProtocol(){
        Url newUrl = new Url("://launchcode.org/hi");
    }

    //TODO: Throw an error if domain is empty
    @Test(expected = IllegalArgumentException.class)
    public void emptyDomain(){
        Url newUrl = new Url("https:///hi");
    }

    //TODO: Throw an error if domain contains invalid characters
    @Test(expected = IllegalArgumentException.class)
    public void invalidDomain(){
        Url newUrl = new Url("https://11,1.com/hi");
    }

    //TODO: Throw an error if port is invalid
    @Test(expected = IllegalArgumentException.class)
    public void invalidPort(){
        Url newUrl = new Url("https://111.com:65536/hi");
    }

    //TODO: Throw an error if query invalid
    @Test(expected = IllegalArgumentException.class)
    public void invalidQuery(){
        Url url = new Url("https://111.com/hi?key=value&value=key&zdadf====");
    }

    @Test
    public void testDomainAllowedCharacters() {
        String urlStr = "https://abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-1234567890.org";
        Url url = new Url(urlStr);
        assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz_-1234567890.org", url.getDomain());
    }


}
