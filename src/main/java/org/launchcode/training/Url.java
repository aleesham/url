package org.launchcode.training;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Url {

    private final String protocol, domain, path, port, query, fragment, url;
    private final static ArrayList<String> accepted_protocols = new ArrayList<>(Arrays.asList(new String[]{"ftp", "http", "https", "file"}));

    public Url(String url) {
        this.url = url;
        this.protocol = extractProtocol(url.toLowerCase());
        this.domain = extractDomain(url.toLowerCase());
        this.path = extractPath(url.toLowerCase());
        this.port = extractPort(url);
        this.query = extractQuery(url);
        this.fragment = extractFragment(url);
    }

    private String extractProtocol(String url){
        List<String> urlList = Arrays.asList(url.split("://")); //[protocol, ...the rest]
        String protocol = urlList.get(0);


        if(protocol.replace(" ", "") == ""){
            throw new IllegalArgumentException("Cannot have an empty protocol");
        }

        if(!accepted_protocols.contains(protocol)){
            throw new IllegalArgumentException("This protocol is not accepted");
        }
        return protocol;
    }

    private String extractDomain(String url){
        List<String> urlList = Arrays.asList(url.split("/")); //[protocol:, empty, domain, ...the rest]
        String domain = urlList.get(2);

        if(domain.matches("[a-z_.-]+:[0-9]+")){
            domain = domain.split(":")[0];
        }

        if(domain.equals("")){
            throw new IllegalArgumentException("Cannot have an empty domain");
        }

        //This works for the above as well if we do not want to specify the error with a message.
        if(!domain.matches("[a-z0-9_.-]+")) {
            throw new IllegalArgumentException("Invalid characters in domain");
        }



        return domain;
    }

    private String extractPort(String url){

        String domain = url.split("/")[2],
                port = "";
        if(domain.matches("[a-z0-9_.-]+:[0-9]+")){
            port = Arrays.asList(domain.split(":")).get(1);
            if(!(0<=Integer.parseInt(port) && Integer.parseInt(port)<65536)){
                throw new IllegalArgumentException("Invalid port");
            }
        } else {
            String protocol = this.getProtocol();
            switch(protocol){
                case "http":
                    port = "80";
                    break;
                case "https":
                    port = "443";
                    break;
                case "ftp":
                case "file":
                    port = "21";
            }
        }
        return port;
    }

    private String extractPath(String url){

        String[] urlList = url.split("/"); //[protocol, empty, domain, ...the rest]
        int listLen = urlList.length;
        String[] domainList = Arrays.copyOfRange(urlList, 3, listLen);

        String output = "";

        for(String piece : domainList){
            if(piece.contains("?")){
                piece = piece.split("[?]")[0];
            }
            output += "/" + piece;
        }
        if(url.endsWith("/")){
            output += "/";
        }
        return output;
    }

    private String extractQuery(String url){
        String[] urlList = url.split("/"); //[protocol, empty, domain, ...the rest]
        int listLen = urlList.length;
        String possibleQuery = urlList[listLen-1];
        if(!possibleQuery.contains("?")){
            return "";
        }
        possibleQuery = possibleQuery.split("[?#]")[1];
        for(String keyValuePair : possibleQuery.split("&")){
            //This is more complicated in reality, or at least I think it is.
            if(keyValuePair.matches("^([a-zA-z]+[=]{2,})")){
                throw new IllegalArgumentException("Invalid query string");
            }
        }
        return possibleQuery;
    }

    private String extractFragment(String url){
        //TODO: Need to implement validation

        String[] urlList = url.split("/"); //[protocol, empty, domain, ...the rest]
        int listLen = urlList.length;
        String possibleFragment= urlList[listLen-1];
        if(!possibleFragment.contains("#")){
            return "";
        }
        String[] queryFragmentList = possibleFragment.split("[?#]");
        return queryFragmentList[queryFragmentList.length-1];
    }


    public String toString(){
        String query = "", fragment = "", port = "";
        if(this.getQuery() != ""){
            query = "?" + this.getQuery();
        }
        if(this.getFragment() != ""){
            fragment = "#" + this.getFragment();
        }
        if(!this.usesDefaultPort()){
            port = ":" + this.getPort();
        }
        return this.getProtocol() + "://" + this.getDomain() + port + this.getPath() + query + fragment;
    }

    public boolean usesDefaultPort(){
        String domain = this.getUrl().split("/")[2];
        if(domain.matches("[a-z0-9_.-]+:[0-9]+")) {
            return false;
        }
        return true;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public String getPort() {
        return port;
    }

    public String getQuery() {
        return query;
    }

    public String getFragment() {
        return fragment;
    }

    public String getUrl() {
        return url;
    }
}
